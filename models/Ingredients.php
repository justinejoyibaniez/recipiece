<?php

namespace app\models;

use Yii;
use app\models\Recipe;
/**
 * This is the model class for table "tblingredients".
 *
 * @property int $id
 * @property int $recipe_id
 * @property string $name
 * @property int $quantity
 *
 * @property Tblrecipe $recipe
 */
class Ingredients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tblingredients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'name', 'quantity'], 'required'],
            [['recipe_id'], 'integer'],
            [['name', 'quantity'], 'string', 'max' => 255],
            [['recipe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipe::className(), 'targetAttribute' => ['recipe_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recipe_id' => 'Recipe ID',
            'name' => 'Name',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * Gets query for [[Recipe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecipe()
    {
        return $this->hasOne(Tblrecipe::className(), ['id' => 'recipe_id']);
    }
}
