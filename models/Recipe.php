<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblrecipe".
 *
 * @property int $id
 * @property string $title
 * @property int $size
 * @property string $_procedure
 * @property string $date_created
 *
 * @property Tblingredients[] $tblingredients
 */
class Recipe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tblrecipe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'size', '_procedure'], 'required'],
            [['size'], 'integer'],
            [['_procedure'], 'string'],
            [['date_created'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'size' => 'Serving size',
            '_procedure' => 'Procedure',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[Tblingredients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTblingredients()
    {
        return $this->hasMany(Ingredients::className(), ['recipe_id' => 'id']);
    }

    public function getAllingredients($id)
    {
        return Ingredients::find()
        ->where(['recipe_id' => $id])
        ->all();
    }
}
