-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2022 at 02:28 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_recipiece`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblingredients`
--

CREATE TABLE `tblingredients` (
  `id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblingredients`
--

INSERT INTO `tblingredients` (`id`, `recipe_id`, `name`, `quantity`) VALUES
(7, 6, 'chicken cut into serving pieces', '2 lbs'),
(8, 6, 'dried bay leaves', '3 pieces '),
(9, 6, 'soy sauce', '8 tablespoons '),
(10, 6, 'white vinegar', '4 tablespoons '),
(11, 6, 'garlic crushed', '5 cloves '),
(12, 6, 'water', '1 1/2 cups '),
(13, 6, 'cooking oil', '3 tablespoons '),
(14, 6, 'sugar', '1 teaspoon '),
(15, 6, 'salt (optional)', '1/4 teaspoon '),
(16, 6, 'whole peppercorn', '1 teaspoon ');

-- --------------------------------------------------------

--
-- Table structure for table `tblrecipe`
--

CREATE TABLE `tblrecipe` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `_procedure` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblrecipe`
--

INSERT INTO `tblrecipe` (`id`, `title`, `size`, `_procedure`, `date_created`) VALUES
(6, 'Filipino Chicken Adobo', 10, 'Cooking Chicken Adobo is quick and simple. This recipe suggests marinating the chicken to make it more flavorful. It is the best way to go if you want to experience authentic Filipino chicken adobo. If you are in a hurry, feel free to skip this step, but make sure to simmer the chicken longer than 30 minutes to better extract the flavors from it.\r\n\r\nHome » Recipes » Filipino Chicken Adobo Recipe\r\nFilipino Chicken Adobo Recipe\r\n Jump to Recipe  Print Recipe\r\nChicken Adobo is a type of Filipino chicken stew. Chicken pieces are marinated in soy sauce and spices, pan-fried, and stewed until tender. The dish gained popularity because of its delicious taste and ease in preparation.\r\n\r\nADVERTISEMENT\r\n\r\n\r\n\r\nChicken Adobo Recipe\r\n\r\nA dish that is classically and quintessentially Pinoy, chicken adobo is a dish that is beloved by many across the country. While there have been several variations of adobo across the years, chicken adobo remains one of the most popular –– and for good reason!\r\n\r\nThe taste of juicy chicken in a succulent, umami adobo sauce makes chicken adobo a comfort food for all ages. From kids to adults, we can’t get enough of this mouthwatering meal!\r\n\r\nADVERTISEMENT\r\n\r\nWhat is Adobo Chicken?\r\nA dish and cooking process native to the Philippines, adobo refers to the method of marinating meat, seafood, or vegetables (pretty much anything!) in a combination of soy sauce and vinegar. This marinade also includes other herbs and flavorings like garlic, bay leaves, and whole peppercorns.\r\n\r\nCooking food in vinegar is no foreign concept to us Filipinos. In pre-colonial times, our ancestors used to cook seafood in vinegar in order to preserve their freshness. Many regard adobo as a spin on kinilaw, which is another traditional cooking method. Kinilaw refers mainly to cooking raw seafood in vinegar and spices. Another similar process is paksiw, which utilizes meat broth in vinegar and spices.\r\n\r\nADVERTISEMENT\r\n\r\nWhat really sets adobo apart is the presence of soy sauce in its marinade. While vinegar has a pungent aroma and a very distinctly sour taste, soy sauce is on both the sweeter and saltier side. A staple in any Asian household, soy sauce (or toyo) definitely helps in bringing out chicken adobo’s savory taste.\r\n\r\n(Here’s a fun fact: did you know that there are different kinds of soy sauce? In Japan especially, there are five different kinds of soy sauce that each have their own unique flavors and uses. The most common one you’ll find in markets is dark soy sauce, or koikuchi. With a deeper color than most other types, dark soy sauce is packed with flavor –– perfect for your chicken adobo!)\r\n\r\nAdobo also contains dry bay leaves in its recipe. Although you aren’t to eat them whole, bay leaves lend their subtle, deep flavors to this umami dish. It may not be the star of the show, but your chicken adobo wouldn’t be complete without it. However, you can choose to substitute this herb with basil if you can’t find it at stores.\r\n\r\nChicken Adobo Origin\r\nThe famous Chicken Adobo originated in the Philippines. The dish is prepared using the Inadobo style of cooking. It means cooking meat or seafood with vinegar and mostly soy sauce. It is a popular method during the olden days when refrigerators and freezers were not yet available because vinegar helps extend the shelf life of food. Another popular variation is pork adobo using pork belly,\r\n\r\nHow to Cook Chicken Adobo\r\nCooking Chicken Adobo is quick and simple. This recipe suggests marinating the chicken to make it more flavorful. It is the best way to go if you want to experience authentic Filipino chicken adobo. If you are in a hurry, feel free to skip this step, but make sure to simmer the chicken longer than 30 minutes to better extract the flavors from it.\r\n\r\nStart by marinating the chicken in soy sauce and garlic. The garlic needs to be crushed for best results. This process takes 1 hour to 12 hours depending on how flavorful you want the dish to be. Sometimes marinating for an hour is not enough. I think that 3 hours is optimal. The chicken absorbs most of the flavors from the soy sauce and garlic during this step. It is noticeable when you taste the dish after cooking. Note that it is also possible to include the vinegar in this step.\r\nThe next step is to separate the chicken from the marinade. Make sure to set the marinade aside because it will be used later on. Pan-fry the chicken pieces for 1 to 1 ½ minutes per side. This will partially cook the outer part. It also makes the skin tough enough to withstand stewing later. This means that it will remain intact, which is nice for presentation.\r\n\r\nPour marinade into the pot and add water. Let boil. The bay leaves and whole peppercorn can now be added. The process takes 20 to 25 minutes depending on the quality of the chicken. However, feel free to cook longer in low heat for a super tender chicken adobo.\r\n\r\nAdd the vinegar. This can also be added as a part of the marinade. Let it cook for 10 minutes and then add sugar and salt. I only add salt if needed. It is important to taste your dish before adding seasonings.\r\n\r\nFilipino chicken adobo can be served with or without sauce. If you like it very tasty then continue to cook on an uncovered cooking pot until the liquid completely evaporates.', '2022-01-12 13:23:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblingredients`
--
ALTER TABLE `tblingredients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_ingredients` (`recipe_id`);

--
-- Indexes for table `tblrecipe`
--
ALTER TABLE `tblrecipe`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblingredients`
--
ALTER TABLE `tblingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tblrecipe`
--
ALTER TABLE `tblrecipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tblingredients`
--
ALTER TABLE `tblingredients`
  ADD CONSTRAINT `recipe_ingredients` FOREIGN KEY (`recipe_id`) REFERENCES `tblrecipe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
