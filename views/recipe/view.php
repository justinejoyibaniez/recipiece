<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Recipe */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Recipes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?><br>
<div class="recipe-view box box-success well">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

   

<div class="panel panel-info">
    <div class="panel-heading text-bold">Recipe Information
</div>
    <div class="panel-body">
        <table class="table table-hover table-bordered table-condensed table-striped" style="border-left: #dddddd 2px solid;">
            <tbody>
            <tr>
                <td width="20%"><b>Title</b></td>
                <td><?= $model->title ?></td>
            </tr>
            <tr>
                <td><b>Serving size</b></td>
                <td><?= $model->size ?> <i class="fa fa-group"></i></td>
            </tr>

            <tr>
                <td><b>Procedure</b></td>
                <td><?= $model->_procedure ?></td>
            </tr>
            <tr>
                <td><b>Date Created</b></td>
                <td><?= date('M d, Y h:i A',strtotime($model->date_created)); ?>    
                </td>
            </tr>
         </tbody>
        </table>
    </div>
</div>
<div class="panel panel-success">
    <div class="panel-heading text-bold">Ingredients</div>
    <div class="panel-body">
        <table class="table table-condensed table-hover table-bordered" style="border-left: #dddddd 2px solid;">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Quantity</th>
                    <th>Ingredients</th>
                    
                </tr>
            </thead>
            <tbody>
                    <?php 
                    $ingredients = $model->getAllingredients($model->id);
                    
                    //$i_ctr = 1;
                    foreach ($ingredients as $key => $ingredient) : 
                        $key ++;?>
                        <tr>
                            <td><?= $key ?></td>
                            <td><?= $ingredient->quantity ?></td>
                            <td><?= $ingredient->name ?></td> 
                            
                        </tr>
                    <?php endforeach; ?>
             </tbody>
        </table>
    </div>
</div>
</div>
