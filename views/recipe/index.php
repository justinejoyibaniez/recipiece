<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecipeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recipe';
$this->params['breadcrumbs'][] = $this->title;
?><br>
<div class="recipe-index box box-success well">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Recipe', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'title',
            'size',
            '_procedure:ntext',
            [
                'attribute' => 'date_created',
                'label' => 'Date Created',
                'value' => function($model){
                    return date('M d, Y',strtotime($model->date_created));
                },
                 'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'name' => 'date_created', 
                    'attribute' => 'date_created',
                    'type' => DatePicker::TYPE_INPUT, 
                    'options' => ['autocomplete' => 'off'],
                    'pluginOptions' => [
                        'format' => 'M d, yyyy',
                        'autoclose' => true,
                        'clearBtn' => true,
                    ] 
                ]),
                  
                'format'=>'raw',


            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
