<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">Recipiece </span>', \yii\helpers\Url::to(['/recipe/index']), ['class' => 'logo']) ?>

    
</header>
